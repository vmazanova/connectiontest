﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionTest : MonoBehaviour
{
    public Text debugText;

    public void TryToConnect()
    {
        if (NewServer.ApiCall.IsConnected())
        {
            debugText.text = "CONNECTED";
        }
        else
        {
            debugText.text = "DISCONNECTED";
        }

        debugText.enabled = true;
    }
}
