﻿using System.IO;
using System.Net;

namespace NewServer
{
    public class ApiCall
    {

        public const string ENDPOINT = "http://mobilegames.powerplay.studio/graphql";

        public const int TIMEOUT = 5000;

        public static string GetHtmlFromUri()
        {
            string html = string.Empty;
            
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(ENDPOINT);
            req.Timeout = TIMEOUT;

            try
            {
                using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
                {
                    bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                    if (isSuccess)
                    {
                        
                        using (TextReader reader = new StreamReader(resp.GetResponseStream()))
                        {
                            char[] cs = new char[10];
                            reader.Read(cs, 0, cs.Length);
                            foreach (char ch in cs)
                            {
                                html += ch;
                            }
                        }
                    }
                }
            }
            catch
            {
                return "";
            }
            
            return html;
        }

        public static bool IsConnected()
        {
            string HtmlText = GetHtmlFromUri();
            if (HtmlText == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}